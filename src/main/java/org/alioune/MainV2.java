package org.alioune;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;

import org.alioune.model.Localisation;
import org.alioune.model.Records;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;

public class MainV2 {

	public static void main(String[] args) throws JsonParseException, JsonMappingException, IOException {
		// TODO Auto-generated method stub
		ObjectMapper objectMapper=new ObjectMapper();
		InputStream input=new FileInputStream("commerce.txt");
		Records records=objectMapper.readValue(input, Records.class);
		System.out.println(records);
		File file=new File("where.js");
		try(FileWriter fr=new FileWriter(file);
		BufferedWriter bw=new BufferedWriter(fr);){
			bw.write("myData = [");
			bw.newLine();
			Localisation[] lc=records.getRecords();
			for(int i=0;i<lc.length;i++) {
				String value="";
				double[] xy=lc[i].getFields().getXy();
				value="["+xy[0]+","+xy[1]+","+"\""+lc[i].getFields().getLibact()+"\""+"]";
				if(i<lc.length-1)
					value+=",";
				bw.write(value);
				bw.newLine();
			}
			bw.write("];");
		}

	}

}
