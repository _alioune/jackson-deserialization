package org.alioune.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
@JsonIgnoreProperties(ignoreUnknown = true)
public class Fields {
	private double[] xy;
	private String libact;

	public double[] getXy() {
		return xy;
	}

	public void setXy(double[] xy) {
		this.xy = xy;
	}
	

	public String getLibact() {
		return libact;
	}

	public void setLibact(String libact) {
		this.libact = libact;
	}

	@Override
	public String toString() {
		String value=xy[0]+" "+xy[1]+"";
		return "Coordonnées: [xy=" + value + "] Type de commerce: "+libact;
	}
    
	

}
