package org.alioune.model;


import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
@JsonIgnoreProperties(ignoreUnknown = true)
public class Records {
	private Localisation[] records;

	public Localisation[] getRecords() {
		return records;
	}

	public void setRecords(Localisation[] records) {
		this.records = records;
	}

	@Override
	public String toString() {
		String resultat="";
		for(Localisation localisation:records) {
			resultat+=localisation.toString();
		}
		return resultat;
	}

}

