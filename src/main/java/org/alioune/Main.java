package org.alioune;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import org.alioune.model.Employee;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
public class Main {
	public static void main(String[] args) throws JsonParseException, JsonMappingException, IOException {
		ObjectMapper objectMapper=new ObjectMapper();
		InputStream input=new FileInputStream("employee.txt");
		Employee em=objectMapper.readValue(input, Employee.class);
		System.out.println(em);
		}

}
